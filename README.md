# Home-sharing-app

Server side technologies:
 - Node.js, express.js
 - Typescript
 - GraphQL API with Apollo Server
 - MongoDB
 - Google Sign in(OAuth2.0)
 - Google's Geocode API
 - Cloudinary

 Client side technologies:
 - React.js
 - React Hooks
 - React Router
 - Graphql, Apollo Client
 - Typescript
 - Ant Design UI
 


