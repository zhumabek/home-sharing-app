import React, {useEffect, useRef, useState} from 'react';
import { useMutation } from "react-apollo";
import {BrowserRouter, Switch, Route} from "react-router-dom";
import {Home, Host, Listing, Listings, Login, NotFound, User} from "./sections";
import {Affix, Layout, Spin} from 'antd';
import "./styles/index.css";
import {Viewer} from "./lib/types";
import {AppHeader} from "./components/AppHeader";
import {LOG_IN} from "./lib/graphql/mutations/LogIn";
import {LogIn as LogInData, LogInVariables } from "./lib/graphql/mutations/LogIn/__generated__/LogIn";
import {ErrorBanner} from "./components/ErrorBanner";
import {AppHeaderSkeleton} from "./components/AppHeaderSkeleton";


const initialViewer: Viewer = {
    id: null,
    token: null,
    avatar: null,
    hasWallet: null,
    didRequest: false
};

function App() {
    const [viewer, setViewer] = useState<Viewer>(initialViewer);
    const [logIn, { error}] = useMutation<LogInData, LogInVariables>(LOG_IN, {
        onCompleted: data => {
            if (data && data.logIn) {
                setViewer(data.logIn);

                if (data.logIn.token) {
                    sessionStorage.setItem("token", data.logIn.token);
                } else {
                    sessionStorage.removeItem("token");
                }
            }
        }
    });

    const logInRef = useRef(logIn);

    useEffect(() => {
        logInRef.current()
    }, []);

    if (!viewer.didRequest && !error) {
        return (
            <Layout className="app-skeleton">
                <AppHeaderSkeleton />
                <div className="app-skeleton__spin-section">
                    <Spin size="large" tip="Launching the app" />
                </div>
            </Layout>
        );
    }

    const logInErrorBannerElement = error ? (
        <ErrorBanner description="We weren't able to verify if you were logged in. Please try again later!" />
    ) : null;

    return (
          <BrowserRouter>
              <Layout id="app">
                  {logInErrorBannerElement}
                  <Affix offsetTop={0} className="app__affix-header">
                      <AppHeader viewer={viewer} setViewer={setViewer} />
                  </Affix>
                  <Switch>
                      <Route exact path="/" component={Home} />
                      <Route exact path="/host" component={Host} />
                      <Route exact path="/listing/:id" component={Listing} />
                      <Route exact path="/listings/:location?" component={Listings} />
                      <Route exact path="/login"
                             render={props => <Login {...props} setViewer={setViewer}/> }
                      />
                      <Route exact path="/user/:id" component={User} />
                      <Route component={NotFound} />
                  </Switch>
              </Layout>
          </BrowserRouter>
  );
}

export default App;
